import MapView from "@trailstash/map-view";
import "@trailstash/map-view/dist/index.css";
import Mordor from "./style.js";

const root = window.location.origin + window.location.pathname;

const view = new MapView({
  map: {
    container: "map",
    zoom: 9,
    center: [-77.1786, 37.4171],
    bearing: -55,
    style: new Mordor({ root }),
  },
  controls: [
    { type: "NavigationControl" },
    {
      type: "GeolocateControl",
      options: {
        positionOptions: {
          enableHighAccuracy: true,
        },
        trackUserLocation: true,
      },
    },
  ],
});
view.map.fitBounds(
  [
    [-77.518921, 37.23976],
    [-76.75302, 37.6966],
  ],
  {
    bearing: -55,
    duration: 0,
  }
);

// register service worker for PWA
if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("./sw.js").then(function (registration) {
    registration.update();
    console.log("Service Worker Registered");
  });
}
