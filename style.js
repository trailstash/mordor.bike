import * as pmtiles from "pmtiles";
import { noLabels } from "protomaps-themes-base";
import data from "./mordor.geojson";

export class Mordor {
  constructor({ root, tileUrl }) {
    this.root = root;
    this.tileUrl = tileUrl;
  }
  setupMapLibreGL(maplibregl) {
    let protocol = new pmtiles.Protocol();
    maplibregl.addProtocol("pmtiles", protocol.tile);
  }
  build() {
    return {
      id: "mordor.bike",
      version: 8,
      name: "mordor.bike",
      sources: {
        protomaps: {
          attribution: `<a href="https://protomaps.com">Protomaps</a> | <a href="https://www.openstreetmap.org/copyright">© OpenStreetMap</a>`,
          type: "vector",
          url: `pmtiles://${this.root}protomaps.pmtiles`,
        },
        mordor: {
          type: "geojson",
          data,
        },
      },
      sprite: `${this.root}sprites`,
      glyphs: `${this.root}fonts/{fontstack}/{range}.pbf`,
      layers: [
        ...noLabels("protomaps", "black"),
        {
          id: "route-casing",
          source: "mordor",
          type: "line",
          paint: {
            "line-color": "black",
            "line-width": 6,
          },
        },
        {
          id: "route",
          source: "mordor",
          type: "line",
          paint: {
            "line-color": "white",
            "line-width": 2,
          },
        },
        {
          id: "poi",
          source: "mordor",
          type: "symbol",
          filter: ["==", ["geometry-type"], "Point"],
          layout: {
            "icon-image": ["get", "type"],
            "text-anchor": "left",
            "text-field": "{name}",
            "text-font": ["Open Sans Regular"],
            "text-justify": "left",
            "text-offset": [1, 0],
            "text-size": 20,
          },
          paint: {
            "text-color": "white",
            "text-halo-color": "black",
            "text-halo-width": 2,
          },
        },
      ],
    };
  }
}
export default Mordor;
